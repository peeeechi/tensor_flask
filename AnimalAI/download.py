from flickrapi import FlickrAPI
from urllib.request import urlretrieve
from pprint import pprint
import os, time, sys, yaml
base = os.path.dirname(os.path.abspath(__file__))

# API Key Get
with open(os.path.join(base, "../FlicerAPI_Key.yaml"), 'r', encoding='utf-8') as rf:
    yamlData = yaml.load(rf)

key         = yamlData['Key']
secret      = yamlData['Secret']
wait_time   = 1

# 保存フォルダの指定
animalnames  = sys.argv[1:]

filckr      = FlickrAPI(key, secret, format='parsed-json')

for name in animalnames:
    savedir     = os.path.join(base, name)
    result = filckr.photos.search(
        text        = name,             # 検索キーワード
        per_page    = 400,              # 何件取得するか
        media       = 'photos',         # 検索するメディアの種類
        sort        = 'relevance',      # 検索の関連順に並べる
        safe_search = 1,                # 有害コンテンツは表示しない
        extras      = 'url_q, licence'  # 取得したいデータ url_q: 画像へのパス, licence: ライセンスキー
    )

    photos      = result['photos']
    # 結果を表示
    # pprint(photos)

    for i, photo in enumerate(photos['photo']):
        url_q = photo['url_q']
        filepath = savedir + '/' + photo['id'] + '.jpg'
        if os.path.exists(filepath):
            continue
        urlretrieve(url_q, filepath)
        time.sleep(wait_time)
    
    print("{0} Download Done".format(name))

print('All Download Done')


