from PIL import Image
import os, glob
import numpy as np
from sklearn import model_selection
# from sklearn import cross_validation

base = os.path.dirname(os.path.abspath(__file__))

classes         = ['monkey', 'boar', 'crow']
num_classes     = len(classes)
image_size      = 50

# 画像の読み込み

X = []
Y = []

for idx, clsName in enumerate(classes):
    photos_dir = os.path.join(base, clsName)
    # photos_dir = "./" + clsName
    files = glob.glob(photos_dir + "/*.jpg") # 全てのjpgファイル

    for i, picFile in enumerate(files):
        if i >= 200:
            break
        image = Image.open(picFile)
        image = image.convert("RGB")
        image = image.resize((image_size, image_size))
        data  = np.asarray(image)
        X.append(data)
        Y.append(idx)

X = np.array(X)
Y = np.array(Y)


x_train, x_test, y_train, y_test = model_selection.train_test_split(X, Y)
xy = (x_train,x_test, y_train, y_test)
np.save(os.path.join(base, "./anmal.npy"),xy)

print('done')